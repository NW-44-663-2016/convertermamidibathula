﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DataMamidibathula.Models
{
    public class Movie
    {
        public int MovieId { get; set; }

        [Display(Name = "Movie Name")]
        public string Name { get; set; }

        // the single movie location (e.g. the location of the studio headquarters)


        [ScaffoldColumn(true)]
        public int? LocationID { get; set; }

        public virtual Location Location { get; set; }


        // a list of all places the movie was filmed

        public List<Location> filmSets { get; set; }

        public static List<Movie> ReadAllFromCSV(string filepath)
        {
            List<Movie> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => Movie.OneFromCsv(v))
                                        .ToList();
            return lst;
        }

        public static Movie OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Movie item = new Movie();

            int i = 0;
            item.Name = Convert.ToString(values[i++]);
            item.LocationID = Convert.ToInt32(values[i++]);

            return item;
        }
    }
}
