﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace DataMamidibathula.Models
    

{
    public static class AppSeedData
    {

        public static void Initialize(IServiceProvider serviceProvider, string appPath)
        {
            string relPath = appPath + "//Models//SeedData//";
            var context = serviceProvider.GetService<AppDbContext>();

            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }

            context.Movie.RemoveRange(context.Movie);
            context.Locations.RemoveRange(context.Locations);
            context.SaveChanges();

            SeedLocationsFromCsv(relPath, context);
            SeedMoviesFromCsv(relPath, context);


            //var loc1 = new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" };
            //var loc2 = new Location() { Latitude = 37.6991993, Longitude = -97.4843859, Place = "Wichita", State = "Kansas", Country = "USA" };
            //var loc3 = new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" };
            //var loc4 = new Location() { Latitude = 45.0993041, Longitude = -93.1007215, Place = "Whte Bear Lake", State = "Minnesota", Country = "USA" };
            //var loc5 = new Location() { Latitude = 17.4126272, Longitude = 78.2676166, Place = "Hyderabad", Country = "India" };
            //var loc6 = new Location() { Latitude = 25.9019385, Longitude = 84.6797775, Place = "Bihar", Country = "India" };
            //context.Locations.AddRange(loc1, loc2, loc3, loc4, loc5, loc6);
            //context.SaveChanges();



            //context.AddRange(
            //    new Movie() { Name = "m1", LocationID = loc1.LocationID },
            //      new Movie() { Name = "m1", LocationID = loc2.LocationID }

            //    );
            //context.SaveChanges();

            //context.Movies.AddRange(
            // new Movie() { Name = "m1", LocationID = loc3.LocationID },
            //   new Movie() { Name = "m1", LocationID = loc4.LocationID }

            // );
            //context.SaveChanges();

        }

        private static void SeedMoviesFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "movie.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            Movie.ReadAllFromCSV(source);
            List<Movie> lst = Movie.ReadAllFromCSV(source);
            context.Movie.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        private static void SeedLocationsFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "location.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<Location> lst = Location.ReadAllFromCSV(source);
            context.Locations.AddRange(lst.ToArray());
            context.SaveChanges();
        }







    }
}

